﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coinpaprinka_API
{
    class Devise
    {
        public string deviseID { get; set; }
        public string deviseName { get; set; }

        public Devise(string DeviseID, string DeviseName)
        {
            deviseID = DeviseID;
            deviseName = DeviseName;
        }

    }
}
