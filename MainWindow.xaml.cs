﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coinpaprinka_API
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<Devise> deviseListDestination = new List<Devise>();
            deviseListDestination.Add(new Devise("", ""));
            deviseListDestination.Add(new Devise("usd-us-dollars", "USD"));
            deviseListDestination.Add(new Devise("eth-ethereum", "Ethereum"));
            deviseListDestination.Add(new Devise("xrp-xrp", "XRP"));
            comboDestination.DisplayMemberPath = "deviseName";
            comboDestination.SelectedValuePath = "deviseID";
            comboDestination.ItemsSource = deviseListDestination;

            List<Devise> deviseListSource = new List<Devise>();
            deviseListSource.Add(new Devise("", ""));
            deviseListSource.Add(new Devise("btc-bitcoin", "Bitcoin"));
            deviseListSource.Add(new Devise("eur-euro-token", "Euro"));
            deviseListSource.Add(new Devise("ncc-neurochain", "Neurochain"));
            comboSource.DisplayMemberPath = "deviseName";
            comboSource.SelectedValuePath = "deviseID";
            comboSource.ItemsSource = deviseListSource;

        }

        private async void BtnConvertir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = new CoinpaprikaAPI.Client();
                var converionResult = await client.ConvertAsync(comboSource.SelectedValue.ToString(), comboDestination.SelectedValue.ToString(), decimal.Parse(txtAmount.Text.ToString()));

                lblOutputInfos.Content = txtAmount.Text + " " + comboSource.Text + " égal à ";
                lblPrice.Content = String.Format("{0:0.00}", converionResult.Value.Price) + " " + comboDestination.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
